const User = require('../models/User');

async function handlePatchUsers(req, res) {
	try {
		const { newPassword, oldPassword } = req.body;
		console.log(newPassword, oldPassword );
		const user = await User.findById(req.user.id);
		if (user.password === oldPassword) {
			await User.findByIdAndUpdate(req.user.id, { password: newPassword }, (err) => {
				if (err) {
					return res.status(400).json({ message: err.message });
				}
				res.status(200).json({ message: 'Success' });
			})
		}
		res.status(400).json({ message: 'Incorrect password'});
	} catch (e) {
		res.status(500).json({ message: e.message });
	}
}

async function handleGetUsers(req, res) {
	try {
		await User.findById(req.user.id, (err, data) => {
			if (err) {
				return res.status(400).json({ message: 'User not found'});
			}
			res.status(200).json({
				user:
				{
					_id: data._id,
					username: data.username,
					createdDate: data.createdDate
				}
			});
		});
	} catch (e) {
		res.status(500).json({ message: e.message });
	}
}

async function handleDeleteUsers(req, res) {
	try {
		const id = req.user.id;
		await User.findOneAndRemove({ _id: id }, (err) => {
			if (err) {
				return res.status(400).json({ status: err.message });
			}
			res.status(200).json({ message: 'Success'});
		});
	} catch (e) {
		res.status(500).json({ message: e.message });
	}
}

module.exports = {
	handlePatchUsers,
	handleGetUsers,
	handleDeleteUsers
}