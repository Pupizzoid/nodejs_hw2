const User = require('../models/User');
const jwt = require('jsonwebtoken');
const config = require('config');
const SECRET = config.get('jwtSecret');

async function handleAuthRegister(req, res) {
	try {
		const { username, password } = req.body;
		const candidate = await User.findOne({ username, password });
		if (candidate) {
			return res.status(400).json({ message: `Such user already exists` });
		}
		const date = new Date();
		const user = new User({ username, password, createdDate: date.toLocaleString()});
		await user.save();
		res.status(200).json({ message: 'Success'});

	} catch (e) {
		res.status(500).json({ message: e.message });
	}
}

async function handleAuthLogin (req, res) {
	try {
		const { username, password } = req.body;
		const user = await User.findOne({ username, password });
		if (!user) {
			return res.status(400).json({ message: `User with this email or password not found` });
		}
		res.status(200).json({jwt_token: jwt.sign(({id: user.id}), SECRET)} );
	} catch (e) {
		res.status(500).json({ message: e.message });
	}
}

function handleGetAuth(req, res) {
	res.status(200);
	res.json({});
}

module.exports = {
	handleAuthRegister,
	handleAuthLogin,
	handleGetAuth
}