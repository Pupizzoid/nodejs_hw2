const Note = require('../models/Note');

async function handleAddNotes(req, res) {
	try {
		const { text } = req.body;
		if (text === '') {
			res.status(400).json({ message: "Note wasn't created" });
		}
		const date = new Date();
		const noteData = new Note({ text, userId: req.user.id, createdDate: date.toLocaleString(), completed: false });
		const newNote = await noteData.save();
		if (!newNote) {
			res.status(400).json({ message: "Note wasn't created" });
		}
		res.status(200).json({ message: 'Success', note: newNote});
	} catch (e) {
		res.status(500).json({ message: e.message});
	}
}

async function handlePutNotes(req, res) {
	try {
		const { text } = req.body;
		const id = req.params.id;
		await Note.findByIdAndUpdate(id, { text }, (err) => {
			if (err) {
				return res.status(400).json({ message: err.message });
			}
			res.status(200).json({ message: 'Success' });
		});
	} catch (e) {
		res.status(500).json({ message: e.message });
	}
}

async function handleGetNotes(req, res) {
	try {
		await Note.find({ userId: req.user.id }, (err, data) => {
			if (err) {
				return res.status(400).json({message: err.message});
			}
			res.status(200).json({notes: data});
		});
	} catch (e) {
		res.status(500).json({ message: e.message });
	}
}

async function handleDeleteNotes(req, res) {
	try {
		const { id } = req.params;
		await Note.findOneAndRemove({ _id: id }, (err) => {
			if (err) {
				return res.status(400).json({ message: err.message });
			}
			res.status(201).json({ message: 'Success'});
		});
	} catch (e) {
		res.status(500).json({ message: e.message });
	}
}

async function handleNoteById(req, res) {
	try {
		const { id } = req.params;
		await Note.findById({ _id: id }, (err, data) => {
			if (err) {
				return res.status(400).json({ message: err.message });
			}
			res.status(200).json({ note: data});
		});
	} catch (e) {
		res.status(500).json({ message: e.message });
	}
}
async function handlePatchNotes(req, res) {
	try {
		const id = req.params.id;
		const note = await Note.findById(id);
		try {
			await Note.findByIdAndUpdate(id, {completed: !note.completed})
			res.status(200).json({ message: 'Success' });
		} catch (err) {
			res.status(400).json({ message: err.message });
		}
	} catch (e) {
		res.status(500).json({ message: e.message });
	}
}

module.exports = {
	handleAddNotes,
	handlePutNotes,
	handleGetNotes,
	handleDeleteNotes,
	handleNoteById,
	handlePatchNotes
}