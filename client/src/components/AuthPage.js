import React, { useState, useContext, useEffect } from 'react';
import AuthContext from '../context/authContext';
import api from '../api';
import { makeStyles } from '@material-ui/core/styles';
import {
	CardContent,
	Card,
	Typography,
	Button,
	TextField,
	Box
} from '@material-ui/core';

const useStyles = makeStyles({
  root: {
		width: 500,
		marginTop: 100,
		margin: 'auto'
  },
  title: {
		fontSize: 36,
		textAlign: 'center',
		marginBottom: 50
	},
	btnContainer: {
		width: '50%',
		display: 'flex',
		justifyContent: 'space-between',
		margin: 'auto',
	},
	form: {
		width: '100%',
		display: 'flex',
		flexWrap: 'wrap',
	},
	fieldContainer: {
		width: '100%',
		display: 'flex',
		marginBottom: 50,
		justifyContent: 'space-between',
	}
});

const AuthPage = () => {
	const classes = useStyles();
	const { setAuth } = useContext(AuthContext);
	const [form, setForm] = useState({ username: '', password: '' });

	useEffect(() => {
		try {
			api.token().then((data) => {
				if (data.status === 'Invalid JWT') {
					return setAuth(false);
				}
				setAuth(true);
			})
		} catch (e) {
			console.log('No Auth')
		}
	}, [setAuth]);

	const setUserDataToStorage = (jwt_token) => {
		localStorage.setItem('token', jwt_token);
	}

	const handlerChangeInput = (e) => {
		setForm({ ...form, [e.target.name]: e.target.value });
	}

	const handleSubmitRegisterData = () => {
		api.register(form);
		setForm({ username: '', password: '' });
	}

	const handleSubmitLoginData = () => {
		api.login(form).then((...data) => {
			if (data[0].jwt_token) {
				setAuth(true);
				setUserDataToStorage(data[0].jwt_token);
			}
		});
		setForm({ username: '', password: '' });
	}

	return (
		<Box>
			<Card className={classes.root}>
				<CardContent>
					<Typography
						color="textSecondary"
						component="h2"
						className={classes.title}>
						Authorization
					</Typography>
					<Box className={classes.form}>
						<Box className={classes.fieldContainer}>
							<TextField
								id="username"
								label="Username"
								variant="outlined"
								name="username"
								value={form.username}
								onChange={handlerChangeInput}
							/>
							<TextField
								id="password"
								label="Password"
								variant="outlined"
								placeholder="******"
								name="password"
								value={form.password}
								onChange={handlerChangeInput}
							/>
						</Box>
						<Box className={classes.btnContainer}>
							<Button
								variant="contained"
								color="primary"
								onClick={handleSubmitRegisterData}>
								Register
							</Button>
							<Button
								variant="contained"
								color="secondary"
								onClick={handleSubmitLoginData}>
								Login
							</Button>
						</Box>
					</Box>
				</CardContent>
			</Card>
		</Box>
	)
}

export default AuthPage;