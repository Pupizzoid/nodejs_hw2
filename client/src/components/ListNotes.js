import React, { useState, useEffect } from 'react';
import ListNotesContext from '../context/ListNotesContext';
import { Link } from 'react-router-dom';
import api from '../api';
import { makeStyles } from '@material-ui/core/styles';
import {
	Box,
	ListItem,
	Typography,
	Button,
	List,
	TextField,
	Card,
	CardContent,
	Icon
} from '@material-ui/core';

const useStyles = makeStyles({
  root: {
		width: 500,
		marginTop: 100,
		margin: 'auto'
  },
  title: {
		fontSize: 36,
		textAlign: 'center',
		marginBottom: 50
	},
	field: {
		width: '80%'
	},
	form: {
		display: 'flex',
		justifyContent: 'space-around',
		alignContent: 'center',
		marginBottom: 30
	},
	link: {
		width: '100%',
		textAlign: 'right',
		display: 'block',
		textDecoration: 'none',
		fontSize: 20,
		color: '#303f9f'
	},
	subTitle: {
		fontSize: 18,
		textAlign: 'center'
	},
	noteCard: {
		width: '100%',
		cursor: 'pointer'
	},
	content: {
		display: 'flex',
		justifyContent: 'space-between',
		alignContent: 'center',
	},
	text: {
		fontSize: 16
	},
	buttonContainer: {
		width: '15%',
		display: 'flex',
		justifyContent: 'space-between'
	}
});

const ListNotes = () => {
	const [newNote, setNewNote] = useState('');
	const [dataNotes, setDataNotes] = useState([]);
	const value = { dataNotes, setDataNotes };
	const classes = useStyles();

	const handleSubmitNoteData = (e) => {
		e.preventDefault();
		if (newNote === '') {
			return;
		}
		api.postNote({
			text: newNote,
		})
			.then(data => {
				if (data.message === 'Success') {
					api.getNotes().then((...data) => {
						const newData = data[0].notes;
						setDataNotes([...newData]);
					})
				}
		})
		setNewNote('');
	}

	const handleChangeNoteInput = (e) => {
		setNewNote(e.target.value);
	}

	const handleDeleteNote = (id) => {
		api.deleteNote(id)
			.then(data => {
				console.log(data);
				if (data.message === 'Success') {
					api.getNotes().then((...data) => {
						console.log(data);
						const newData = data[0].notes;
						setDataNotes([...newData]);
					})
				}
		})
	}

	const drawNotes = dataNotes.map(note => {
			return (
				<ListItem key={note._id}>
					<Card className={classes.noteCard}>
						<CardContent className={classes.content}>
						<Typography
							color="textSecondary"
							component="p"
							className={classes.text}>
							{note.text}
							</Typography>
							<Box className={classes.buttonContainer}>
								<Icon
									color="primary"
									onClick={() => handleDeleteNote(note._id)}
									className={classes.icon}>
									delete
								</Icon>
								<Link to={`/note/${note._id}`} className={classes.link}>
									<Icon
										color="primary"
										className={classes.icon}>
										visibility
									</Icon>
								</Link>
							</Box>
						</CardContent>
						</Card>
				</ListItem>
			)
		});

	useEffect(() => {
		try {
			api.getNotes().then((...data) => {
				const newData = data[0].notes;
				setDataNotes([...newData]);
			})
		} catch (e) {
			console.log('No notes');
		}
	}, []);

	return (
		<ListNotesContext.Provider value={value}>
			<Box className={classes.root}>
				<Link
					to='/profile'
					className={classes.link}>
					Go to profile
				</Link>
				<Typography
					color="textSecondary"
					component="h2"
					className={classes.title}>
					Your Notes
				</Typography>
				<form
					className={classes.form}
					onSubmit={handleSubmitNoteData}>
					<TextField
						label="Note"
						name='email'
						value={newNote}
						placeholder="Enter your notes"
						className={classes.field}
						onChange={handleChangeNoteInput}
					/>
					<Button
						variant="contained"
						color="primary"
						type='submit'>
						Add
					</Button>
				</form>
				<Box>
					{drawNotes.length < 1 ?
						<Typography
							color="textSecondary"
							component="h4"
							className={classes.subTitle}>
							You don't have any notes!
						</Typography> :
						<List>
							{drawNotes}
						</List>
					}
				</Box>
			</Box>
		</ListNotesContext.Provider>
	)
}

export default ListNotes;