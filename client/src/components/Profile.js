import React, {useEffect, useState, useContext} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AuthContext from '../context/authContext';
import { Link, useHistory } from 'react-router-dom';
import api from '../api';
import {
	CardContent,
	Card,
	Typography,
	Button,
	ButtonGroup,
	Box,
	TextField
} from '@material-ui/core';

const useStyles = makeStyles({
  root: {
		width: 500,
		marginTop: 100,
		margin: 'auto'
  },
  title: {
		fontSize: 36,
		textAlign: 'center',
		marginBottom: 50
	},
	btnContainer: {
		width: '90%',
		display: 'flex',
		justifyContent: 'space-between',
		margin: 'auto',
	},
	boxInfo: {
		marginBottom: 30
	},
	note: {
		fontSize: 16
	},
	link: {
		width: '100%',
		textAlign: 'right',
		display: 'block',
		textDecoration: 'none',
		fontSize: 20,
		color: '#303f9f'
	},
	form: {
		width: '100%',
		display: 'flex',
		justifyContent: 'space-between'
	},
	field: {
		flexGrow: 1,
		paddingRight: 15,
	}
});

const Profile = () => {
	const [userInfo, setUserInfo] = useState({ username: '', password: '' });
	const { setAuth } = useContext(AuthContext);
	const [isEditPasswordState, setEditPasswordState] = useState(false);
	const [passwords, setPasswords] = useState({
		oldPassword: '',
		newPassword: ''
	});
	const history = useHistory()


	useEffect(() => {
		try {
			api.getUser()
				.then(data => {
					setUserInfo({
						...userInfo,
						username: data.user.username
					});
			})
		} catch (e) {
			setAuth(false);
		}
	}, []);

	const handleAuthLogout = () => {
		localStorage.removeItem('token');
		setAuth(false);
		history.push('/');
	}

	const handleDeleteUserAccount = () => {
		api.deleteUser();
		localStorage.removeItem('token');
		setAuth(false);
		history.push('/');
	}

	const handleChangePassword = () => {
		setEditPasswordState(true);
	}

	const handleSubmitNewPassword = (e) => {
		e.preventDefault();
		const { newPassword, oldPassword } = passwords;
		if (newPassword === '' || oldPassword === '') {
			return;
		}
		api.patchUser(passwords);
		setEditPasswordState(false);
		setPasswords({
			oldPassword: '',
			newPassword: ''
		});
	}

	const handleChangePasswordInput = (e) => {
		setPasswords({ ...passwords, [e.target.name]: e.target.value });
	}

	const classes = useStyles();
	return (
		<Card className={classes.root}>
			<CardContent>
				<Link
					to='/notes'
					className={classes.link}>
					See my notes
				</Link>
				<Typography
					color="textSecondary"
					component="h2"
					className={classes.title}>
					Profile
				</Typography>
				<Box className={classes.boxInfo}>
				{!isEditPasswordState ?
					<Typography
						color="textSecondary"
						component="p">
					Username: {userInfo.username}
					</Typography> :
					<form
						className={classes.form}
						onSubmit={handleSubmitNewPassword}>
						<TextField
							label="Old password"
							name='oldPassword'
							value={passwords.oldPassword}
							className={classes.field}
							onChange={handleChangePasswordInput}
							/>
						<TextField
							label="New password"
							name='newPassword'
							value={passwords.newPassword}
							className={classes.field}
							onChange={handleChangePasswordInput}
						/>
						<Button
							variant="contained"
							color="primary"
							type='submit'>
							Save
						</Button>
					</form>
				}
				</Box>
				<ButtonGroup
					variant="contained"
					color="primary"
					aria-label="contained primary button group"
					className={classes.btnContainer}>
					<Button onClick={handleChangePassword}>Change Password</Button>
					<Button onClick={handleDeleteUserAccount}>Delete Account</Button>
					<Button onClick={handleAuthLogout}>Logout</Button>
				</ButtonGroup>
			</CardContent>
		</Card>
	)
}

export default Profile;