import React, { useState, useContext, useEffect } from 'react';
import { useParams, Link}from 'react-router-dom';
import api from '../api';
import ListNotesContext from '../context/ListNotesContext';
import { makeStyles } from '@material-ui/core/styles';
import {
	Typography,
	Button,
	TextField,
	Box,
	Icon,
	Checkbox,
	Card,
	CardContent,
} from '@material-ui/core';


const useStyles = makeStyles({
	root: {
		width: '50%',
		margin: 'auto',
		marginTop: 50
	},
	text: {
		fontSize: 16
	},
	content: {
		width: '100%',
		display: 'flex',
		alignContent: 'center',
	},
	textContainer: {
		width: '80%'
	},
	form: {
		width: '100%',
		display: 'flex',
		justifyContent: 'space-between'
	},
	field: {
		flexGrow: 1,
		paddingRight: 15
	},
	checkText: {
		textDecoration: 'line-through'
	},
	icon: {
		cursor: 'pointer'
	},
	link: {
		width: '100%',
		textAlign: 'right',
		display: 'block',
		textDecoration: 'none',
		fontSize: 20,
		color: '#303f9f',
		marginBottom: 30
	}
});


const Note = () => {
	const [isEdit, setIsEdit] = useState(false);
	const [note, setNote] = useState({
		text: '',
		completed: false,
		_id: ''
	});

	const {dataNotes, setDataNotes } = useContext(ListNotesContext);
	const classes = useStyles();
	const { id } = useParams();

	useEffect(() => {
		try {
			api.getNote(id).then((...data) => {
				const newNote = data[0].note;
				setNote({ ...note, ...newNote });
			})
		} catch (e) {
			console.log('No notes');
		}
	}, []);

	const handleEditNote = () => {
		setIsEdit(true);
	}

	const handleChangeNoteInput = (e) => {
		setNote({...note, text: e.target.value})
	}

	const handleCheckNote = () => {
		api.patchNote(id)
			.then((data) => {
				if (data.message === 'Success') {
					setNote({ ...note, completed: !note.completed });
				}
			}).catch((err) => {
			console.log(err);
		})
	}

	const handleSubmitEditedNote = (e) => {
		e.preventDefault();
		api.putNote(id, {text: note.text});
		setIsEdit(false);
	}

	const addIsCheckClassName = () => {
		return !note.completed ? classes.text : classes.checkText;
	}

	return (
		<Box className={classes.root}>
			<Link to='/notes' className={classes.link}>To notes list</Link>
			<Card>
			{!isEdit ?
				<CardContent className={classes.content}>
					<Box className={classes.textContainer}>
						<Checkbox
							checked={note.completed}
							onChange={handleCheckNote}
							inputProps={{ 'aria-label': 'primary checkbox' }}
						/>
						<Typography
							color="textSecondary"
							component="span"
							className={addIsCheckClassName()}>
							{note.text}
						</Typography>
					</Box>
					<Box>
						<Icon
							color="primary"
							onClick={handleEditNote}
							className={classes.icon}>
							edit
						</Icon>
					</Box>
				</CardContent> :
				<CardContent>
					<form
						className={classes.form}
						onSubmit={handleSubmitEditedNote}>
						<TextField
							label="Note"
							name='note'
							value={note.text}
							className={classes.field}
							onChange={handleChangeNoteInput}
						/>
						<Button
							variant="contained"
							color="primary"
							type='submit'>
							Save
						</Button>
					</form>
				</CardContent>
			}
			</Card>
		</Box>
	)
}

export default Note;
