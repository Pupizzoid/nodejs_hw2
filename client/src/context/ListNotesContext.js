import { createContext } from 'react';

const ListNotesContext = createContext({
	listNotes: false,
	setListNotes: () => {}
});

export default ListNotesContext;