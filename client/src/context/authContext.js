import { createContext } from 'react';

const AuthContext = createContext({
	isAuth: false,
	setAuth: () => {}
});

export default AuthContext;