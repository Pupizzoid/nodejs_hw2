import React from 'react';
import Profile from './components/Profile';
import ListNotes from './components/ListNotes';
import Note from './components/Note';
import { Switch, Route, Redirect }from 'react-router-dom';
import AuthPage from './components/AuthPage';

export const userRoutes = (isAuth) => {
	if (isAuth) {
		return (
			<Switch>
				<Route path='/profile' exact>
					<Profile/>
				</Route>
				<Route path='/notes' exact>
					<ListNotes/>
				</Route>
				<Route path='/note/:id' exact>
					<Note/>
				</Route>
				<Redirect to='/notes' />
			</Switch>
		)
	}
	return (
		<Switch>
			<Route path='/'>
				<AuthPage />
			</Route>
			<Redirect to='/'/>
		</Switch>
	)
}
