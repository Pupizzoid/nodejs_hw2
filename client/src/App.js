import React, {useState} from 'react';
import { userRoutes } from './routes';
import { BrowserRouter } from 'react-router-dom';
import Box from '@material-ui/core/Box';
import AuthContext from './context/authContext';

const App = () => {
	const [isAuth, setAuth] = useState(false);
	const value = { isAuth, setAuth };
	const routes = userRoutes(isAuth);
	return (
		<AuthContext.Provider value={value}>
			<BrowserRouter>
				<Box>
					{routes}
				</Box>
			</BrowserRouter>
		</AuthContext.Provider>
  );
}

export default App;
