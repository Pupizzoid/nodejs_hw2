import { http } from './http.js';

const api = {
	register: (user) => {
		return http.post(`/api/auth/register`, user, false);
	},
	login: (user) => {
		return http.post(`/api/auth/login`, user, false );
	},
	token: () => {
		return http.get(`/api/auth`);
	},
	postNote: (text) => {
		return http.post(`/api/notes`, text);
	},
	deleteNote: (id) => {
		return http.delete(`/api/notes/${id}`);
	},
	putNote: (id, text) => {
		return http.put(`/api/notes/${id}`, text);
	},
	patchNote: (id) => {
		return http.patch(`/api/notes/${id}`);
	},
	getNotes: () => {
		return http.get(`/api/notes`);
	},
	getNote: (id) => {
		return http.get(`/api/notes/${id}`);
	},
	getUser: () => {
		return http.get(`/api/users/me`);
	},
	deleteUser: () => {
		return http.delete(`/api/users/me`);
	},
	patchUser: (passwords) => {
		return http.patch(`/api/users/me`, passwords);
	},
};

export default api;
