const requestHeaders = {
	'Content-Type': 'application/json'
};

const http = {
	post: async (url, body, isAuthReq = true) => {
		if (isAuthReq) {
			requestHeaders['Authorization'] = 'Basic ' + localStorage.getItem('token');
		}
		try {
			const bodyMessage = JSON.stringify(body);
			const res = await fetch(url, { method: 'POST', body: bodyMessage, headers: requestHeaders });
			const data = await res.json();
			return data;
		} catch (e) {
			throw e;
		}
	},
	put: async (url, body) => {
		requestHeaders['Authorization'] = 'Basic ' + localStorage.getItem('token');
		try {
			console.log(body);
			const	bodyMessage = JSON.stringify(body);
			const res = await fetch(url, { method: 'PUT', body: bodyMessage, headers: requestHeaders });
			const data = await res.json();
			return data;
		} catch (e) {
			throw e;
		}
	},
	get: async (url) => {
		requestHeaders['Authorization'] = 'Basic ' + localStorage.getItem('token');
		try {
			const res = await fetch(url, { method: 'GET', headers: requestHeaders });
			const data = await res.json();
			return data;
		} catch (e) {
			throw e;
		}
	},
	delete: async (url) => {
		requestHeaders['Authorization'] = 'Basic ' + localStorage.getItem('token');
		try {
			const res = await fetch(url, { method: 'DELETE', headers: requestHeaders });
			const data = await res.json();
			return data;
		} catch (e) {
			throw e;
		}
	},
	patch: async (url, body) => {
		requestHeaders['Authorization'] = 'Basic ' + localStorage.getItem('token');
		try {
			const bodyMessage = JSON.stringify(body);
			const res = await fetch(url, { method: 'PATCH', body: bodyMessage, headers: requestHeaders });
			const data = await res.json();
			return data;
		} catch (e) {
			throw e;
		}
	}
}

export {
	http
} 