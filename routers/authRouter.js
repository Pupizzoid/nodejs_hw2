const { Router } = require('express');
const router = Router();
const authMiddleWare = require('../middleWares/authMiddleWare');
const { handleAuthRegister, handleAuthLogin, handleGetAuth } = require('../controllers/authControllers');


router.post('/auth/register', handleAuthRegister);

router.post('/auth/login', handleAuthLogin);

router.get('/auth', authMiddleWare, handleGetAuth)

module.exports = router;