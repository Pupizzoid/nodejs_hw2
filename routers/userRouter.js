const { Router } = require('express');
const router = Router();
const authMiddleWare = require('../middleWares/authMiddleWare');
const { handleGetUsers, handlePatchUsers, handleDeleteUsers} = require('../controllers/usersControllers');


router.get('/users/me',authMiddleWare, handleGetUsers);
	
router.patch('/users/me',authMiddleWare, handlePatchUsers);
	
router.delete('/users/me', authMiddleWare, handleDeleteUsers);

module.exports = router;
