const { Router } = require('express');
const router = Router();
const authMiddleWare = require('../middleWares/authMiddleWare');
const {
	handleAddNotes,
	handleGetNotes,
	handlePutNotes,
	handleDeleteNotes,
	handleNoteById,
	handlePatchNotes
} = require('../controllers/notesControllers');


router.post('/notes', authMiddleWare, handleAddNotes);

router.get('/notes', authMiddleWare, handleGetNotes);

router.get('/notes/:id', authMiddleWare, handleNoteById);
	
router.put('/notes/:id', authMiddleWare, handlePutNotes);

router.patch('/notes/:id', authMiddleWare, handlePatchNotes);
	
router.delete('/notes/:id', authMiddleWare, handleDeleteNotes);

module.exports = router;