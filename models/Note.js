const { Schema, model} = require('mongoose');

const schema = new Schema({
	text: {
		type: String,
		required: true,
	},
	completed: {
		type: Boolean,
		required: true
	},
	userId: {
		type: String,
		required: true,
	}
});

module.exports = model('Note', schema);